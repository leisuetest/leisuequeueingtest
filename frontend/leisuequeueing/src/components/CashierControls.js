import React from 'react';
import { Row, Col, Container, Button, Card, CardGroup } from 'react-bootstrap';
import './CashierControls.css';
import { Fragment } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import AddQueueModal from './AddQueueModal';

export default function CashierControls(){
    return (
        <Fragment>
            <Container fluid>
                <div className="d-flex justify-content-center">
                    <Row>
                        <Col>
                            <div>Payment</div>
                            <h3>Cashier 4</h3>
                            <div>Single Serving</div>
                        </Col>
                        <Col className="row border d-flex">
                            <AddQueueModal />
                            <div className="col mx-2 my-3 d-flex"><Button>q history</Button></div>
                        </Col>
                    </Row>
                </div>
            </Container>
            <CardGroup>
                <Card>
                    <Card.Header>
                        <Card.Title>On-going Queue</Card.Title>
                        <Card.Text>List of all on-going customer queues</Card.Text>
                    </Card.Header>
                    <Card.Header>
                        <Card.Subtitle>Queue Number | Customer Info | Type/Time Queued | Options | Serve</Card.Subtitle>
                    </Card.Header>
                </Card>
            </CardGroup>
        </Fragment>
    );
}