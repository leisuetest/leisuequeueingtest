import {useEffect, useState } from 'react';
import QueueContainerCard from './QueueContainerCard';

export default function QueueContainer(){
    const [queue, setQueue] = useState([]);
    console.log('before fetching')
    useEffect(()=>{
        fetch('http://localhost:4000/customers/getQueue')
        .then(res=>res.json())
        .then(data => {
            console.log(data)
            setQueue(data.map(customer=>{
                return(
                    <QueueContainerCard QueueContainerCardProp={customer} key={customer._id}/>
            )
            }))
        }).catch(err=>console.log(err))
    },[])
    return(
        <>{queue}</>
    )
}


