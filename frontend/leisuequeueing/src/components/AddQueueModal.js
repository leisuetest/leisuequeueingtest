import { useState, useEffect } from 'react';
import { Button, Modal, Form, FloatingLabel } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddQueueModal(){
    const [fullName, setFullName] = useState("");
    const [contactNumber, setContactNumber] = useState("");
    const [numOfPeople, setNumOfPeople] = useState("");
    const [isPriorityQ, setIsPriorityQ] = useState(false);

    const [show, setShow] = useState(false);

    const handleClose = () =>  {
        setShow(false);
    }

    const handleSubmit = (e) =>{
        fetch(`http://localhost:4000/customers/addQueue`, {
            method: 'POST',
            body: JSON.stringify({
                fullName: fullName,
                contactNumber: contactNumber,
                numOfPeople: numOfPeople,
                isPriorityQ: isPriorityQ
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            (data===true)?
            Swal.fire({
                icon:'success',
                title: 'Customer added to queue',

            })
            :
            Swal.fire({
                icon: 'error',
                title: 'Something went wrong',
            })
        })
    }


    const handleShow = () => {
        setShow(true);
    }

    return (
        <>
            <Button variant="primary" onClick={handleShow}>Add Queue</Button>
            <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Add Queue<br/>
                        <small>fill-up fields with the customer's data</small>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <div>Customer Details</div>
                        <FloatingLabel controlId="contactNumber" label="Mobile Number">
                            <Form.Control type="text" value={contactNumber} onChange={(e) => setContactNumber(e.target.value)}/>
                        </FloatingLabel>
                        <FloatingLabel controlId="contactNumber" label="How many people?">
                            <Form.Control type="number" value={numOfPeople} onChange={(e) => setNumOfPeople(e.target.value)}/>
                        </FloatingLabel>
                        <Form.Control type="text" value={fullName} placeholder='Name(Optional)' onChange={(e) => setFullName(e.target.value)}/>
                        <Form.Check reverse type="switch" label="Priority Queue" checked={isPriorityQ} onChange={(e) => setIsPriorityQ(e.target.checked)}/>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                    <Button variant="primary" onClick={handleSubmit}>Submit</Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}