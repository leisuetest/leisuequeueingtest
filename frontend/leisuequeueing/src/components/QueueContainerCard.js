import { useEffect, useState } from 'react';
import { Button, Card } from 'react-bootstrap';
import PropTypes from 'prop-types'
import Swal from 'sweetalert2';


export default function QueueContainerCard({QueueContainerCardProp}){
    const {_id, fullName, contactNumber, numOfPeople, isPriorityQ, queueNumber, timeQueued} = QueueContainerCardProp;
    const formattedTimeQueued = new Date(timeQueued).toLocaleString()

    const handleServeClick = (e) => {
        fetch(`http://localhost:4000/customers/${_id}/serve`,{
            method: 'POST',
            'Content-Type': 'application/json'
        })
        .then(res=>{
            Swal.fire({
                icon:'success',
                title: 'Serving',
                timer: 1500
            })
            
        })
    }
    return(
        <Card className="m-auto" id="QueueContainerCard">
            <Card.Body>
                <Card.Title>A00{queueNumber}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                    {fullName}
                </Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">
                    {contactNumber}
                </Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">
                    Number of People: {numOfPeople}
                </Card.Subtitle>
                <div>
                    <Card.Subtitle>
                        Queue Type: {isPriorityQ? 'Priority' : 'Normal'}
                    </Card.Subtitle>
                    <Card.Subtitle>
                        Time Queued: {formattedTimeQueued}
                    </Card.Subtitle>
                </div>
            </Card.Body>
            <Card.Footer>
                <Button variant="warning" onClick={handleServeClick}>Serve</Button>
            </Card.Footer>
        </Card>
    )

}