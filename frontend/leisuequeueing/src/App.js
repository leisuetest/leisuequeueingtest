import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'

//? dependencies
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

//? pages
import Main from './pages/Main';

//? context

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Main/>}/>
      </Routes>
    </Router>
  )
}

export default App;
