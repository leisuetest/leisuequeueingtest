import { Fragment } from 'react';
import CashierControls from '../components/CashierControls';
import 'bootstrap/dist/css/bootstrap.min.css'
import QueueContainer from '../components/QueueContainer';
import CurrentServingContainer from '../components/CurrentServingContainer';

export default function Main() {
    return(
        <Fragment>
            <>
                <CashierControls />
            </>
            <><QueueContainer /></>
            <br/>
            <>Current Serving Container Placeholder<CurrentServingContainer /></>
        </Fragment>
    )
}