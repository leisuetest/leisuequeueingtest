const Customer = require('../models/Customer');
const bcrypt = require("bcrypt");


//? add queue
module.exports.addQueue = (req, res) => {
    Customer.findOne().sort({ queueNumber: -1 })
        .then(lastCustomer => {
            let newQueueNumber = 1;
            if (lastCustomer) {
                newQueueNumber = lastCustomer.queueNumber + 1;
            }
            let newCustomer = new Customer({
                fullName: req.body.fullName,
                contactNumber: req.body.contactNumber,
                isServed: false,
                numOfPeople: req.body.numOfPeople,
                isPriorityQ: req.body.isPriorityQ,
                queueNumber: newQueueNumber
            })
            Customer.findOne({contactNumber:req.body.contactNumber})
            .then(result=>{
                if(result != null && result.length > 0){
                    res.send(result.json())
                }else{
                    newCustomer.save();
                    res.send(true);
                }
            })
        })
        .catch(err => {
            console.error(err);
            res.status(500).send('Error adding queue');
        });
};

//? list of ongoing Queue
module.exports.getQueue = (req, res) => {
    Customer.find({isServed: false})
    .sort({timeQueued:1})
    .then(customers => {
        res.send(customers);
    })
    .catch(err => {res.status(500).json({err:err.message})})
}

//? move to isServing container
module.exports.serveQueue = (req, res) =>{
    Customer.findByIdAndUpdate(req.params.id,{
        isServed: true},
        {new: true}
    ).then(res.send(true))
    .catch(err => {res.status(500).json({err:err.message})})
}

//? Done Serving

module.exports.doneServing = (req, res) => {
    Customer.findByIdAndUpdate(req.params.id,{
        isDoneServing: true},
        {new: true}
    ).then(res.send(true))
    .catch(err => {res.status(500).json({err:err.message})
    })
}

module.exports.displayQueueCount = (req, res) => {
    Customer.countDocuments({isServed: false}, (err, count)=>{
        if(err){
            console.error(err);
            res.status(500).send(err);
        }else{
            res.send(`There are ${count} customers in the queue`);
        }
    });
}