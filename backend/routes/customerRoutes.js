const express = require("express");
const router = express.Router();
const customerController  = require("../controllers/customerControllers.js");

module.exports = router;

router.post("/addQueue", customerController.addQueue);

router.get("/getQueue", customerController.getQueue);

router.post("/:id/serve", customerController.serveQueue);

router.post("/doneServe", customerController.doneServing);

router.get("/displayQueueCount", customerController.displayQueueCount);