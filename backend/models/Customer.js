const mongoose = require ("mongoose");

const customerSchema = new mongoose.Schema({
    fullName: {
        type: String,
        default: "Customer"
    },
    contactNumber: {
        type: String,
        required: [true, "Contact Number is required"]
    },
    isServed: {
        type: Boolean,
        default: false
    },
    isDoneServing:{
        type: Boolean,
        default: false
    },
    numOfPeople: {
        type: Number,
        required: true
    },
    isPriorityQ: {
        type: Boolean,
        default: false
    },
    timeQueued: {
        type: Date,
        default: Date.now,
        required: true
    },
    timeServed:{
        type:Date,
        default: null,
    },
    queueNumber: {
        type: Number,
        default: 0,
    }
});

module.exports = mongoose.model("Customer", customerSchema);