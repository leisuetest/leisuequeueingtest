const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const autoIncrement = require("mongoose-auto-increment");
const customerRoutes = require('./routes/customerRoutes');

const app = express();

//my middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/customers", customerRoutes);

// mongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch230.dzsjady.mongodb.net/customerQueueing?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
mongoose.connection.once("open", () => console.log("Now connected to Leisue DB test"))

app.listen(process.env.PORT || 4000, () =>{
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});